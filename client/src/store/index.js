import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appsData: Object
  },
  getters: {
    desksArr: state => state.appsData.data,
    tinkoffDataObj: state => state.appsData.data.tinkoff,
    tinkoffDataAppsArr: state => state.appsData.data.tinkoff.applications,
    tinkoffDataAppsArrApple: state => state.appsData.data.tinkoff.applications.filter(c => c.platform === 'iphone' || c.platform === 'apple'),
    tinkoffDataAppsArrAndroid: state => state.appsData.data.tinkoff.applications.filter(c => c.platform === 'android')
  },
  mutations: {
    setData(state, payload) {
      state.appsData = payload;
    }
  },
  actions: {
    async getData({ commit }, payload) {
      let data = { data: { data } }; // деструктурируем полученный json-объект
      data = await axios.get('http://localhost:8080/apps.json', {
        responseType: 'json',
        headers: { 'Content-Type': 'application/json' }
      })
      commit('setData', data, payload);
    }
  }
})
